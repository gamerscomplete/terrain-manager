//TODO: Figure out of distance or mindistance is used for blocking since the comments are effectively the same

package terrainManager

import (
	"gitlab.com/gamerscomplete/terrain-generator"
)

type Config struct {
	GenConfig *terrainGenerator.Config
	//Patches greater than this distance are culled
	MaxDistance int
	//Patches less than this distance are forced generation via blocking to ensure holes in the world do not happen, at the expense of visual lag
	MinDistance int
	//Normal distance for patches to be added to the queue for generation
	Distance int
	//To keep from allowing world holes when getting behind on load, set blocking distance from player
	//If distance is less than this and not loaded, the loading will block instead of just using best effort
	BlockingDistance int
}

var DefaultConfig = &Config{
	GenConfig:        terrainGenerator.DefaultConfig,
	MaxDistance:      15,
	MinDistance:      5,
	BlockingDistance: 2,
	Distance:         10,
}
