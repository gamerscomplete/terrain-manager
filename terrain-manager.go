package terrainManager

import (
	"errors"
	"fmt"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/gls"
	"gitlab.com/g3n/engine/graphic"
	"gitlab.com/g3n/engine/material"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/texture"
	tgen "gitlab.com/gamerscomplete/terrain-generator"
	"golang.org/x/sync/semaphore"
	"math"
	"sync"
)

type TerrainManager struct {
	config    *Config
	generator *tgen.TerrainGenerator

	patchLock sync.RWMutex
	patches   map[math32.Vector2]*graphic.Mesh

	bufferLock sync.RWMutex

	queueLock  sync.RWMutex
	patchQueue []queueRecord

	lastPosition math32.Vector3

	node *core.Node

	terrainMaterial *material.Standard
}

type Priority int

const (
	HIGH_PRIORITY Priority = iota
	LOW_PRIORITY
)

type queueRecord struct {
	pos      math32.Vector2
	priority Priority
	*semaphore.Weighted
}

func NewTerrainManager(config *Config) (*TerrainManager, error) {
	if config == nil {
		//Default all configs
		config = DefaultConfig
	} else if config.GenConfig == nil {
		config.GenConfig = tgen.DefaultConfig
		//Have a config but no terrain generator config, use default
	}

	generator, err := tgen.NewTerrainGenerator(config.GenConfig)
	if err != nil {
		return nil, err
	}
	manager := TerrainManager{generator: generator, config: config, patches: make(map[math32.Vector2]*graphic.Mesh)}
	manager.node = core.NewNode()

	//TODO: This is super shitty. make this not hard coded
	texfile := "assets/terrain.png"
	tex1, err := texture.NewTexture2DFromImage(texfile)
	if err != nil {
		return nil, errors.New("Failed to load terrain texture: " + err.Error())
	}
	tex1.SetWrapS(gls.REPEAT)
	tex1.SetWrapT(gls.REPEAT)
	tex1.SetRepeat(2, 2)

	manager.terrainMaterial = material.NewStandard(&math32.Color{1, 1, 1})
	manager.terrainMaterial.AddTexture(tex1)
	manager.terrainMaterial.SetWireframe(false)
	manager.terrainMaterial.SetSide(material.SideDouble)

	return &manager, nil
}

func (tman *TerrainManager) Node() *core.Node {
	return tman.node
}

func (tman *TerrainManager) HasPatch(pos math32.Vector2) bool {
	tman.patchLock.RLock()
	defer tman.patchLock.RUnlock()
	for key, _ := range tman.patches {
		if key == pos {
			return true
		}
	}
	return false
}

func (tman *TerrainManager) IsQueued(pos math32.Vector2) bool {
	tman.queueLock.RLock()
	defer tman.queueLock.RUnlock()
	for _, value := range tman.patchQueue {
		if value.pos == pos {
			//We already have this patch
			return true
		}
	}
	return false
}

func (tman *TerrainManager) AddPatch(patch tgen.Patch) {
	tman.patchLock.Lock()
	defer tman.patchLock.Unlock()
	fmt.Println("AddPatch not implemented")
}

func (tman *TerrainManager) RemovePatch(patch math32.Vector2) {
	tman.patchLock.Lock()
	defer tman.patchLock.Unlock()
	fmt.Println("RemovePatch not implemented")
}

func (tman *TerrainManager) queuePatch(pos math32.Vector2, priority Priority) {
	if tman.HasPatch(pos) || tman.IsQueued(pos) {
		return
	}
	record := queueRecord{pos: pos, priority: priority, Weighted: semaphore.NewWeighted(1)}
	tman.patchQueue = append(tman.patchQueue, record)
}

func (tman *TerrainManager) dequeuePatch(pos math32.Vector2) {
	tman.queueLock.Lock()
	defer tman.queueLock.Unlock()
	for key, value := range tman.patchQueue {
		if value.pos == pos {
			//We already have this patch
			tman.patchQueue = append(tman.patchQueue[:key], tman.patchQueue[key+1:]...)
			break
		}
	}
}

func (tman *TerrainManager) ProcessQueue() {
	var wg sync.WaitGroup

	for _, value := range tman.patchQueue {
		if value.TryAcquire(1) {
			if value.priority == HIGH_PRIORITY {
				wg.Add(1)
			}
			go func(input queueRecord) {
				if input.priority == HIGH_PRIORITY {
					defer wg.Done()
				}
				defer input.Release(1)
				tman.loadPatch(input.pos)
				tman.dequeuePatch(input.pos)
			}(value)
		}
	}

	wg.Wait()
}

func (tman *TerrainManager) loadPatch(pos math32.Vector2) {
	//Generate patch
	patch := tman.generator.GeneratePatch(pos)
	patch.CreateGeometry()
	meshPatch := graphic.NewMesh(patch, tman.terrainMaterial)

	patchSize := float32(tman.config.GenConfig.PatchSize())
	meshPatch.SetPosition(pos.X*patchSize, 0, pos.Y*patchSize)
	//Add patch to manager
	tman.patchLock.Lock()
	tman.patches[pos] = meshPatch
	tman.patchLock.Unlock()

	tman.patchLock.RLock()
	tman.node.Add(tman.patches[pos])
	tman.patchLock.RUnlock()
}

//Find patches that need to be loaded
func (tman *TerrainManager) Update(position math32.Vector3) {
	//TODO: Make this logic a bit fuzzier. floats have a tendency to float around depending on the precision
	if position == tman.lastPosition {
		return
	}
	tman.bufferLock.Lock()
	defer tman.bufferLock.Unlock()

	//Precompute the length
	//Dropped this since we shouldnt need to set it multiple times. Will need to create it somewhere else tho
	//	tman.patchBuffer = make([]math32.Vector2, ((MAX_DISTANCE * 2) + 1) * ((MAX_DISTANCE * 2) + 1))
	currentPatch := tman.GetPatch(position)

	var tempVector math32.Vector2
	//Determine patches required around position
	//	for x := -tman.config.MinDistance; x <= tman.config.MinDistance; x++ {
	//		for y := -tman.config.MinDistance; y <= tman.config.MinDistance; y++ {
	for x := -tman.config.Distance; x <= tman.config.Distance; x++ {
		for y := -tman.config.Distance; y <= tman.config.Distance; y++ {
			tempVector.Copy(&currentPatch).Add(&math32.Vector2{X: float32(x), Y: float32(y)})
			//check if this patch is already loaded
			if !tman.HasPatch(tempVector) {
				//load the patch
				if math.Abs(float64(y)) < float64(tman.config.MinDistance) {
					tman.queuePatch(tempVector, HIGH_PRIORITY)
				} else {
					tman.queuePatch(tempVector, LOW_PRIORITY)
				}
			}
			//			tman.patchBuffer = append(tman.patchBuffer, tempVector)
		}
	}
	tman.lastPosition = position
	tman.ProcessQueue()
}

func (tman *TerrainManager) GetPatch(position math32.Vector3) math32.Vector2 {
	return math32.Vector2{X: float32(int(position.X) / tman.generator.PatchSize()), Y: float32(int(position.Z) / tman.generator.PatchSize())}
}
